<?php
	include('readjson.php');
	$product = json_decode($json);
?> 

<!DOCTYPE html>
<html lang="en">
	
<head>
	
    <meta charset="utf-8">
    <title>Site Title</title>
    
    <?php include('head.php'); ?>

</head>

	<body>
		
		<?php include("google.php") ?>
	
		<?php include('header.php'); ?>	
	
		<div class="container">
	
		<!-- list products 	-->
		<div class="products">
			
		<?php foreach ($product->data as $indx=>$p){ ?>
			 
			
				 
		     	<div class="product thmb" 
			     	 data-indx="<?=$indx?>" 
				     data-id="<?=$p->attrs->product_id?>">
						 
		
				
			    
				    <!-- image -->
				    <a href="/dead/<?=$p->attrs->handle?>">
					     <div class="image">
						     <img class="variant-image" src="<?=$p->attrs->images[0]->src?>" 
						     	  alt="<?=$p->attrs->vendor . ' ' . $p->attrs->title . ' ' .$p->attrs->product_type?>" />
					     </div>
					</a>  
			
				
					<!-- title 	--> 
					<a href="/dead/<?=$p->attrs->handle?>">     
						<h1 class="product-title"><?=$p->attrs->title?></h1>
					</a>
					<h2 class="variant-title"></h2>
					<div class="variant-price"></div>    
			
				
					<!-- buy --> 
					<div class="buy">
						
						   
					    <div class="options">
						    
						    <?php foreach ($p->attrs->options as $o){ 
							    $options = [];
						    ?>
						     
						     <h4><?=$o->name?></h4>
						     
						     <?php foreach ($p->attrs->variants as $variants){ 
							     
						     	   	 foreach ($variants->option_values as $v){  
							     	   
								     	   if($o->name === $v->name){ 
										 	   $options[] = $v;
										 	}
								  	   
										}
					
							     	} 
							     	
			                        // remove duplicates
							     	$clean = array_unique($options, SORT_REGULAR);
							     	
							     	foreach($clean as $c){ ?>
							     	
								     	<div class="variant-selectors"
										 	 data-name="<?=$c->name?>" 
											 data-value="<?=$c->value?>"><?=$c->value?></div>
											 
							     	<?php }
							     	
							     }
						     ?>
						     
					     </div>
				    
				  
					     <button class="btn buy-button js-prevent-cart-listener">Buy</button>
				     
				     
			     </div>
			
			    
			    
		     </div>
					 
	
		     
		<?php } ?>
		
		</div>
		
		<?php include('cart.php'); ?>
		
	</div>
		
		<?php include('footer.php'); ?>
	
	</body>
</html>