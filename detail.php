<?php
	
	require_once  "_inc/lib/vendor/autoload.php";
	use Flow\JSONPath\JSONPath;
	
	//load product json
	include('readjson.php');
	
	$url = $_REQUEST['url'];
	$jsonObj = json_decode($json);
	$jsonPath = new JSONPath($jsonObj);
	$result = $jsonPath->find('$..[?(@.handle == "'.$url.'")]');
	$p = json_decode(json_encode($result[0]));
	
	$indx = 0;
    foreach($jsonObj->data as $key => $value){
	    if($value->attrs->product_id === $p->product_id){
		    $indx = $key;
	    }
    }
    
?>

<!DOCTYPE html>
<html lang="en">
	
 <head>
	  
    <meta charset="utf-8">
    <title><?=$p->title?></title>
    
    <?php include('head.php'); ?>

 </head>

 <body>
	  
 	<?php include("google.php") ?>
 	
  	<?php include('header.php'); ?>	
  	
  	<div class="container">
	  
	  <div class="product row" 
	  	   data-indx="<?=$indx?>" 
		   data-id="<?=$p->product_id?>">
		
		   
		   <!-- image -->
		   <div class="col-xs-6">
		  	<img class="variant-image img-responsive" 
		  		 src="<?=$p->images[0]->src?>" 
		  		 alt="<?=$p->vendor . ' ' . $p->title . ' ' . $p->product_type?>"/>
		   </div>
	  	   
	  	   <!-- info -->
		   <div class="col-xs-6">
			 
			  	<h1><?=$p->title?></h1>
			  	<p class="description"><?=$p->body_html?></p>
			  	
			  	<h2 class="variant-title"></h2>
				<div class="variant-price">Select a size for price.</div>   
		
			     <!-- buy -->
				<div class="buy">
					
					 <!-- options -->     
				     <div class="options">
					    
					    <?php foreach ($p->options as $o){ 
						    $options = [];
					    ?>
					     
					     <h4><?=$o->name?></h4>
					     
					     <?php foreach ($p->variants as $variants){ 
						     
					     	   	 foreach ($variants->option_values as $v){  
						     	   
							     	   if($o->name === $v->name){ 
									 	   $options[] = $v;
									 	}
							  	   
									}
				
						     	} 
						     	
		                        // remove duplicates
						     	$clean = array_unique($options, SORT_REGULAR);
						     	
						     	foreach($clean as $c){ ?>
						     	
							     	<div class="variant-selectors"
									 	 data-name="<?=$c->name?>" 
										 data-value="<?=$c->value?>"><?=$c->value?></div>
										 
						     	<?php }
						     	
						     }
					     ?>
					     
				     </div>
			     
			     	 <!-- button -->
				     <button class="btn buy-button js-prevent-cart-listener">Buy Now</button>
				     
			     </div>
			  
		     
	  	</div>
	
	  </div> 
		  
	  <?php include('cart.php'); ?>
  </div>
  	
  	<?php include('footer.php'); ?>	
  	
  </body>
 
</html>